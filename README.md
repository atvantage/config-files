<!-- markdownlint-disable MD033 -->
<!-- markdownlint-disable MD041 -->
<!-- *** FOR NOTE *** -->

<!-- PROJECT SHIELDS -->
<!-- -->

<!-- ASSET SOURCE -->
<!--
*** logo: https://www.flaticon.com/premium-icon/configuration_2797635
-->

<br />
<p align="center">
  <a href="">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Bizcuit Config Files</h3>

  <p align="center">
    Internal repo for config files
    <br />
    <a href=""><strong>Explore the docs »</strong></a>
    <br />
    <br />
  </p>
</p>

##### Table of Contents

- [🌐 About The Project](#-about-the-project)
- [✅ Getting Started](#-getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [🏗 Contributing](#-contributing)
- [✏️ License](#️-license)
- [📥 Contact](#-contact)

## 🌐 About The Project

The Internal repository to share configuration files.

## ✅ Getting Started

### Prerequisites

- npm

  ```sh
  npm install npm@latest -g
  ```

- commitizen, cz-emoji

  ```sh
  npm install commitizen cz-emoji -g
  ```

- replace ~/.czrc as your global config for cz-emoji (can use this to update)

  ```sh
  curl https://bitbucket.org/atvantage/config-files/raw/master/scripts/setup-update-czrc.sh | bash
  ```

### Installation

you can just run the following command, will run all

[setup-eslint, setup-prettier, setup-update-czrc, setup-commitlint, setup-husky]

```sh
curl https://bitbucket.org/atvantage/config-files/raw/master/scripts/main-install.sh | bash
```

## 🏗 Contributing

! **NO NEED** to update **cz-emoji-types.json** file (this file will auto update if there are changes in cz-emoji.config.json)

- npm install
- **IMPLEMENT**

- use **cz** to commit (there are husky hooks please follow it)

## ✏️ License

Distributed under the MIT License. See `LICENSE` for more information.

## 📥 Contact

Ice - isoon@bizcuitsolution.com

Group - supachai@bizcuitsolution.com

Anshul - anshul@bizcuitsolution.com

Boss - atichat@bizcuitsolution.com

P - puttiphong@bizcuitsolution.com

Project Link: [bitbucket](https://bitbucket.org/atvantage/config-files/src/master/)

[product-screenshot]: images/screenshot.png
