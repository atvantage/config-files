const fs = require('fs');

const readJsonFile = (file) => {
  try {
    const contentString = fs.readFileSync(file, 'utf8');

    return JSON.parse(contentString);
  } catch (err) {
    console.error(err);
    throw new Error(`💥 [readJsonFile]: something wrong with file: ${file}`);
  }
};

module.exports = readJsonFile;
