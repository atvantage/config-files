const types = require('./cz-emoji-types.json');
const typesEnum = types.map(({ code }) => code);

module.exports = {
  extends: ['gitmoji'],
  rules: {
    'header-max-length': [2, 'always', 75],
    'scope-case': [2, 'always', ['lower-case']],
    'scope-enum': [0],
    'subject-case': [2, 'always', ['lower-case']],
    'subject-empty': [2, 'never'],
    'type-enum': [2, 'always', typesEnum],
  },
  parserPreset: {
    parserOpts: {
      headerPattern:
        /^(:\w*:)(?:\s)(?:\((.*?)\))?\s((?:.*(?=\())|.*)(?:\(#(\d*)\))?/,
      headerCorrespondence: ['type', 'scope', 'subject'],
    },
  },
};
