module.exports = {
  '*.{js,jsx,ts,tsx}': ['eslint --fix', 'eslint'],
  '*.{css,scss,json,md}': ['prettier --write'],
};
