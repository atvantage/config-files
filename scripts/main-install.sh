echo "# ------------------------------------- \ ------------------------------------ #"
curl -s https://bitbucket.org/atvantage/config-files/raw/master/scripts/setup-eslint.sh | bash

echo "# ------------------------------------- \ ------------------------------------ #"
curl -s https://bitbucket.org/atvantage/config-files/raw/master/scripts/setup-prettier.sh | bash

echo "# ------------------------------------- \ ------------------------------------ #"
curl -s https://bitbucket.org/atvantage/config-files/raw/master/scripts/setup-update-czrc.sh | bash

echo "# ------------------------------------- \ ------------------------------------ #"
curl -s https://bitbucket.org/atvantage/config-files/raw/master/scripts/setup-commitlint.sh | bash

echo "# ------------------------------------- \ ------------------------------------ #"
curl -s https://bitbucket.org/atvantage/config-files/raw/master/scripts/setup-husky.sh | bash

# echo "# ------------------------------------- \ ------------------------------------ #"
# curl -s https://bitbucket.org/atvantage/config-files/raw/master/scripts/setup-gitignore.sh | bash

echo "✅ Done main-install.sh (setup-eslint, setup-prettier, setup-update-czrc, setup-commitlint, setup-husky)"
echo "♥️ Have a nice day!"