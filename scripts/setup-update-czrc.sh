curl -s https://bitbucket.org/atvantage/config-files/raw/master/cz-emoji.config.json -o 'cz-emoji.config.json'
echo "✔️ Done dowload/update cz-emoji.config.json to your local"

cat ./cz-emoji.config.json > ~/.czrc
echo "✔️ Done replace ~./czrc"

rm ./cz-emoji.config.json
echo "✔️ Done rm cz-emoji.config.json from your local"

echo "✅ Done setup-cz-emoji"