# -------------------------------- pred items -------------------------------- #
# *Add file here if you need to add in gitignore
items=(
  cz-emoji-types.json
  commitlint.config.js
)
printf -v formmatted_items '\n%s' "${items[@]}"
formmatted_items=${formmatted_items:1}

content="# 🔧 Generated using bizcuits/config-files \n${formmatted_items}"
# check if .gitignore file already exists
if [ ! -f ".gitignore" ]
then
    echo -e "${content}" > .gitignore
    echo "✔️ Successfully created .gitignore"
else
    echo -e "$(cat .gitignore)\n\n${content}" > .gitignore
    echo "✔️ Successfully append to existing .gitignore \n${formmatted_items}"
fi

echo "# ------------------------------------- \ ------------------------------------ #"
echo "✅ Done setup-gitignore"