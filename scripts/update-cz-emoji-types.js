const fs = require('fs');
const readJsonFile = require('../helper/read-json-file.js');

const readNameCode = (file) => {
  try {
    const config = readJsonFile(file);
    const configTypes = config['config']['cz-emoji']['types'];
    const mappedNameCode = configTypes.map(({ name, code }) => ({
      name,
      code,
    }));

    return mappedNameCode;
  } catch (err) {
    console.error(err);
    throw new Error('💥 [readNameCode]: Failed to map name code from file');
  }
};

const updateCZEmojiTypes = () => {
  try {
    const source = 'cz-emoji.config.json';
    const destination = 'cz-emoji-types.json';
    const content = readNameCode(source);

    const isDestExist = fs.existsSync(destination);
    const previousContent = isDestExist ? readJsonFile(destination) : '';

    const contentString = JSON.stringify(content);
    const previousContentString = JSON.stringify(previousContent);
    const isSameAsPreviousContent = contentString === previousContentString;

    if (isSameAsPreviousContent) {
      console.log(`✔ (${source}) nothing changed`);
      return;
    }

    fs.writeFileSync(destination, contentString, {
      flag: 'w+',
    });

    console.log(`✅ success: updated ${source}`);
    console.warn(
      `❗ There are changes ${source}, Please, commit ${destination} as well`,
    );
  } catch (err) {
    console.error(err);
  }
};

updateCZEmojiTypes();
