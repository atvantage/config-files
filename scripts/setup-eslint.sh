npm i -D eslint; eslint-config-airbnb-base; eslint-config-airbnb-typescript; eslint-config-prettier;  eslint-plugin-import;  eslint-plugin-jsx-a11y;  eslint-plugin-prettier; eslint-plugin-unused-imports;

curl -s https://bitbucket.org/atvantage/config-files/raw/master/.eslintrc -o '.eslintrc'
echo "✔️ Done dowload/update .eslintrc to your local"

curl -s https://bitbucket.org/atvantage/config-files/raw/master/.eslintignore -o '.eslintignore'
echo "✔️ Done dowload/update .eslintignore to your local"

echo "✅ Done setup-eslint"