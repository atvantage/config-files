npm install -D sort-package-json lint-staged
curl -s https://bitbucket.org/atvantage/config-files/raw/master/lint-staged.config.js -o 'lint-staged.config.js'
echo "✔️ Done dowload lint-staged.config.js to your local"

npx husky install
npx husky add .husky/commit-msg 'npx --no-install commitlint --edit \"$1\"'
npx husky add .husky/pre-commit 'npx lint-staged; npx sort-package-json;'
# npx husky add .husky/pre-push 'npm run test'
echo "✔️ Done install husky and add commit-msg pre-commit hook"

echo "✅ Done setup-husky"