# ------------------------------ cz-emoji-types ------------------------------ #
curl -s https://bitbucket.org/atvantage/config-files/raw/master/cz-emoji-types.json -o 'cz-emoji-types.json'
echo "✔️ Done dowload/update cz-emoji-types.json to your local"

# -------------------------------- commitlint -------------------------------- #
npm i -D commitlint commitlint-config-gitmoji
curl -s https://bitbucket.org/atvantage/config-files/raw/master/commitlint.config.js -o 'commitlint.config.js'
echo "✔️ Done install commitlint commitlint-config-gitmoji as dev packages"

echo "✅ Done setup-commitlint"